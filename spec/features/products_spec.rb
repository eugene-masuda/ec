require 'rails_helper'

RSpec.describe '商品詳細ページ', type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) do
    create(:custom_product,
           name: "SampleProduct", description: "test", price: "23.45", taxons: [taxon])
  end

  before do
    visit potepan_product_path(product.id)
  end

  describe 'product/showテンプレートの商品詳細' do
    scenario "商品情報が表示され、タイトルがproduct.name - base_titleを返す" do
      within find('.mainContent') do
        expect(page).to have_content product.name
        expect(page).to have_content product.description
        expect(page).to have_content product.price
        expect(page).to have_title product.name
      end
    end

    scenario "一覧ページへ戻るを押すと、商品のカテゴリへリンクする" do
      expect(page).to have_link '一覧ページへ戻る'
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon.id)
      expect(page).to have_content taxon.name
    end
  end
end

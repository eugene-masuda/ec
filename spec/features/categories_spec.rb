require 'rails_helper'

RSpec.describe '商品カテゴリページ', type: :feature do
  let!(:taxonomy)    { create(:taxonomy, name: 'Categories') }
  let!(:rails_taxon) { create(:taxon, name: 'Ruby on Rails', parent_id: taxonomy.root.id) }
  let!(:bags_taxon)  { create(:taxon, name: 'Bags', parent_id: taxonomy.root.id) }
  let!(:product) do
    create(:custom_product,
           name: "SampleProduct", description: "test", price: "23.45", taxons: [rails_taxon])
  end
  let!(:rails_bag) do
    create(:custom_product, name: 'Rails Bag', price: '22.99', taxons: [rails_taxon, bags_taxon])
  end

  describe 'categories/showテンプレートの商品詳細' do
    before do
      visit potepan_category_path(rails_taxon.id)
    end

    scenario 'カテゴリー名や商品情報の記載がある' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end

    scenario "サイドバーにtaxonが表示されること" do
      within first(".panel-default") do
        expect(page).to have_content "#{rails_taxon.name} (#{rails_taxon.all_products.count})"
        expect(page).to have_content "#{bags_taxon.name} (#{bags_taxon.all_products.count})"
      end
    end

    scenario '商品の情報は商品詳細へのリンクになっている' do
      expect(page).to have_link product.display_price
      click_on product.display_price
      expect(current_path).to eq potepan_product_path(product.id)
      expect(page).to have_content product.name
    end
  end
end

require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :request do
  describe "GET #show" do
    let(:taxon)   { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "products/showへのリクエストが成功する" do
      expect(response.status).to eq(200)
    end

    it "products/showテンプレートが表示される" do
      expect(response).to render_template :show
      expect(response.body).to include(product.name)
      expect(response.body).to include(product.price.to_s)
      expect(response.body).to include(product.description)
    end

    it "productが期待された値である" do
      expect(assigns(:product)).to eq product
    end
  end
end
